#!/usr/bin/env python3
import os
import argparse
import json

class Json2SqlQuotes (object):

    def __init__ (self):        
        self.output_sql_files = { "authors" : "authors.sql", 
                                  "topics"  : "topics.sql",
                                  "keywords": "keywords.sql",
                                  "quotes"  : "quotes.sql"
        }

        self.output_error_file = "json_error.log"
        


    @staticmethod
    def parse_args():
        parser = argparse.ArgumentParser(description='doublequotes.org json2sql')
        parser.add_argument("-e","--jsonerror", action='store_true', help="list invalid json files")   
        args = vars( parser.parse_args())

        j2s = Json2SqlQuotes()
        j2s.main(args)

    def main(self, args):                                        
        with open(self.output_error_file, "w") as jsonerrorlog, open(self.output_sql_files["authors"], "w") as authorssql:
            # walk through quotes directory        
            for root, dirs, files in os.walk("quotes"):
                dirs.sort()
                for f in sorted(files):
                    if(f.endswith(".json")):
                        fname = os.path.join(root,f)
                        #print("working on {}".format(fname))
                        with open(fname) as jsonfile:
                            try:
                                jsondata = json.load(jsonfile)
                                if('name' in jsondata): authorssql.write("{}\n".format(jsondata['name']))
                            except json.decoder.JSONDecodeError:
                                if args['jsonerror'] == True: jsonerrorlog.write(fname)
                        #print(json.dumps(jsondata, indent=4))
            for d in dirs:
                #print(os.path.join(root,d))
                pass

        jsonerrorlog.close()
        authorssql.close()    

                


if __name__ == "__main__":
    Json2SqlQuotes.parse_args()